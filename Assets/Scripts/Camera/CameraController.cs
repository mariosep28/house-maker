using UnityEngine;

public class CameraController : MonoBehaviour
{
    // Params
    public float moveSpeed = 0.5f;
    public float riseSpeed = 0.5f;
    public float rotationSpeed = 4f;
    public float panSpeed = 5f;
    public float zoomSpeed = 10f;

    // Inputs
    private float _horizontalInput;
    private float _forwardInput;
    private float _riseInput;
    private float _zoomInput;
    private float _mouseXInput;
    private float _mouseYInput;
    
    // States
    private bool _rotationActive;
    private bool _panActive;

    // Velocities    
    private Vector3 _movementVelocity = new Vector3();
    private Vector3 _panVelocity = new Vector3();
    private Vector3 _riseVelocity = new Vector3();
    
    public float timeToReachTarget = 0.2f;

    private float _minHeight = 3f;    

    private void Update()
    {
        GetInputValues();
        
        // Camera movement
        if(_horizontalInput != 0 || _forwardInput != 0)
        {
            Move();
        }
        
        // Camera rise
        if (_riseInput != 0)
        {
            Rise();    
        }
        
        // Camera zoom
        if (_zoomInput != 0)
        {
            Zoom();
        }
        
        // Camera rotation
        if (_rotationActive)
        {
            Cursor.visible = false;
            
            Rotate();
        }
        else if (_panActive)
        {
            Pan();
        }
        else
        {
            Cursor.visible = true;
        }
        
        LimitMinHeight();
    }

    private void GetInputValues()
    {
        _horizontalInput = InputManager.CameraMovement.x;
        _forwardInput = InputManager.CameraMovement.y;
        _riseInput = InputManager.CameraRise;
        _zoomInput = InputManager.CameraZoom;
        _mouseXInput = InputManager.CameraRotation.x;
        _mouseYInput = InputManager.CameraRotation.y;
        
        _rotationActive = InputManager.CameraRotationActive;
        _panActive = InputManager.CameraPanActive;
    }
    
    private void Move()
    {
        Vector3 direction = (transform.right * _horizontalInput + transform.forward * _forwardInput).normalized;
        Vector3 targetPosition = transform.position + moveSpeed * direction * Time.deltaTime;
        Vector3 smoothMovemevent = Vector3.SmoothDamp(transform.position, targetPosition, ref _movementVelocity, timeToReachTarget);
        transform.position = smoothMovemevent; 
    }

    private void Rise()
    {
        Vector3 direction = (transform.up * _riseInput).normalized;
        Vector3 targetPosition = transform.position + riseSpeed * direction * Time.deltaTime;
        Vector3 smoothRise = Vector3.SmoothDamp(transform.position, targetPosition, ref _riseVelocity, timeToReachTarget);
        transform.position = smoothRise; 
    }

    private void Pan()
    {
        Vector3 xPan = -transform.right * _mouseXInput;
        Vector3 zPan = -transform.up; //-new Vector3(transform.forward.x, 0, transform.forward.z).normalized * _mouseYInput;
        //zPan.y = 0f;
        zPan = zPan.normalized * _mouseYInput;
        
        
        Vector3 direction = (xPan + zPan);
        Vector3 targetPosition = transform.position + panSpeed * direction * Time.deltaTime;
        //Vector3 smoothMovemevent = Vector3.SmoothDamp(transform.position, targetPosition, ref _panVelocity, timeToReachTarget);
        transform.position = targetPosition;
    }

    private void Rotate()
    {
        float yAnglesToRotate = rotationSpeed * _mouseXInput * Time.deltaTime;
        float xAnglesToRotate = rotationSpeed * -_mouseYInput * Time.deltaTime;
        
        Vector3 anglesToRotate = new Vector3(xAnglesToRotate, yAnglesToRotate, 0f);

        Vector3 actualAngle = transform.localEulerAngles;

        Vector3 targetAngle = actualAngle + anglesToRotate;
        targetAngle.z = 0f;

        if (targetAngle.x > 180)
            targetAngle.x -= 360;
        
        targetAngle.x = Mathf.Clamp(targetAngle.x, -80f, 80f);
        
        transform.eulerAngles = targetAngle;
    }

    private void Zoom()
    {
        Vector3 direction =  transform.forward * _zoomInput;
        transform.position += zoomSpeed * direction;
    }

    private void LimitMinHeight()
    {
        Vector3 limitedHeightPosition = transform.position;
        limitedHeightPosition.y = Mathf.Max(limitedHeightPosition.y, _minHeight);
        transform.position = limitedHeightPosition;
    }
}
