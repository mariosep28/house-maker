using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class UIToolSelector : Singleton<UIToolSelector>
{
    public List<Button> toolButtons;

    private void Awake()
    {
        toolButtons = new List<Button>(GetComponentsInChildren<Button>());

        ToolManager.Instance.onNewActiveTool += OnNewActiveTool;
    }

    public void OnToolButtonClicked(ToolButton toolButton)
    {
        ChangeToolCommand command = new ChangeToolCommand(toolButton.Tool);
        CommandInvoker.Instance.AddCommand(command);
    }

    private void OnNewActiveTool(ETools tools)
    {
        Button button = toolButtons.Single(t => t.name == tools.ToString());
        SetButtonSelected(button);
    }

    private void SetButtonSelected(Button buttonSelected)
    {
        foreach (Button toolButton in toolButtons)
            toolButton.interactable = (toolButton != buttonSelected);
    }
}
