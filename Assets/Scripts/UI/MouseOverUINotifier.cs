using System;
using UnityEngine.EventSystems;

public class MouseOverUINotifier : Singleton<MouseOverUINotifier>, IPointerEnterHandler, IPointerExitHandler
{
    public Action onMouseEnterUI;
    public Action onMouseExitUI;
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        onMouseEnterUI?.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        onMouseExitUI?.Invoke();
    }
}
