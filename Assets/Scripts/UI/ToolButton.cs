using UnityEngine;
using UnityEngine.UI;

public class ToolButton : MonoBehaviour
{
    [SerializeField] public ETools tool;
    private Button button;

    public ETools Tool => tool;
    public Button Button => button;
    
    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnToolButtonClicked);
    }

    private void OnValidate()
    {
        gameObject.name = tool.ToString();
    }

    private void OnToolButtonClicked()
    {
        UIToolSelector.Instance.OnToolButtonClicked(this);
    }
}
