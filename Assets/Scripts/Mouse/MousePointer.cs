using UnityEngine;

public class MousePointer : MonoBehaviour
{
    public GameObject pointerPrefab;
    
    private GameObject _pointer;
    
    private void Awake()
    {
        InstantiatePointer();
    }

    private void Update()
    {
        _pointer.transform.position = MousePosition.Instance.worldPosition;
    }

    private void InstantiatePointer()
    {
        _pointer = Instantiate(pointerPrefab, transform);
    }
}
