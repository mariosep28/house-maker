﻿using System;
using Building.Helpers;
using UnityEngine;

namespace PointerHandler
{
    public class Pointer : Singleton<Pointer>
    {
        public Vector3 position;
        public Vector3 positionWithoutSnap;
        
        public GameObject pointerPrefab;
        
        private GameObject _pointer;

        public enum SnapMode
        {
            Wall
        }

        /*public SnapMode snapMode;
        public LayerMask snapLayerMask;
        
        [SerializeField] private GameObject _snappedObject;

        public Action<GameObject> onStartSnapToObject;
        public Action<GameObject> onEndSnapToObject;
        
        public GameObject SnappedObject => _snappedObject;*/
        private void Awake()
        {
            InstantiatePointer();

            /*snapMode = SnapMode.Wall;
            snapLayerMask = LayerMask.GetMask("Wall");*/
        }
        
        private void Update()
        {
            positionWithoutSnap = MousePosition.Instance.worldPosition;
            position = SnapOperations.SnapPosition(positionWithoutSnap);
            
            /*// TODO: Refactor to method or class
            ProximityHit proximityHit;
            
            if (ProximityChecker.IsCloseToGameObject(position, 3f, out proximityHit, snapLayerMask))
            {
                position = proximityHit.closestPoint;

                GameObject currentSnappedObject = proximityHit.collider.gameObject; 
                
                if (_snappedObject != currentSnappedObject)
                {
                    if(_snappedObject != null)
                        onEndSnapToObject?.Invoke(_snappedObject);

                    _snappedObject = currentSnappedObject;
                    
                    onStartSnapToObject?.Invoke(currentSnappedObject);
                }
            }
            else
            {
                if (_snappedObject != null)
                {
                    onEndSnapToObject?.Invoke(_snappedObject);

                    _snappedObject = null;
                }
                    
            }
            */
            _pointer.transform.position = position;
        }
        
        private void InstantiatePointer()
        {
            _pointer = Instantiate(pointerPrefab, transform);
        }
    }
}