using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class MousePosition : Singleton<MousePosition>
{
    public Vector3 worldPosition;
    public Vector3 screenPosition;
    
    private GameObject _objectPointed;
    
    private int _layerMask;

    public Action<GameObject> onStartToPoint;
    public Action<GameObject> onEndToPoint;
    
    public GameObject ObjectPointed => _objectPointed;
    public Vector3 MouseHitNormal => hitData.normal;

    private RaycastHit hitData;

    private void Awake()
    {
        InteractionManager.Instance.onInteractionModeChanged += OnInteractionModeChanged;
    }

    private void Update()
    {
        if(!InteractionManager.Instance.IsOverUI)
            CalculateMousePosition();
    }

    private void OnInteractionModeChanged(InteractionMode  mode)
    {
        switch (mode)
        {
            case InteractionMode.Ground:
                _layerMask = LayerMask.GetMask("Grid");
                break;
            
            case InteractionMode.Wall:
                _layerMask = LayerMask.GetMask("Wall");
                break;
        }
    }
    
    private void CalculateMousePosition()
    {
        screenPosition = Mouse.current.position.ReadValue();
        
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);

        if(Physics.Raycast(ray, out hitData, 1000, _layerMask))
        {
            worldPosition = hitData.point;

            if (_objectPointed != hitData.collider.gameObject)
            {
                if(_objectPointed != null)
                    onEndToPoint?.Invoke(_objectPointed);
                    
                _objectPointed = hitData.collider.gameObject;
                onStartToPoint?.Invoke(_objectPointed);
            }
        }
        else
        {
            if(_objectPointed != null)
                onEndToPoint?.Invoke(_objectPointed);
            
            _objectPointed = null;
        }
    }
}
