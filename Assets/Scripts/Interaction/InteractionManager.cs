using System;
using FSM.PlaceWallTool;

// TODO: Refactor to state machine
public enum InteractionMode
{
    Ground,
    Wall
}

public class InteractionManager : Singleton<InteractionManager>
{
    private InteractionMode _mode;

    private bool _isOverUI;
    
    public InteractionMode Mode => _mode;

    public bool IsOverUI => _isOverUI;
    
    public Action<InteractionMode> onInteractionModeChanged;

    private void Awake()
    {
        ToolManager.Instance.onNewActiveTool += OnNewActiveTool;
        
        MouseOverUINotifier.Instance.onMouseEnterUI += OnMouserEnterUI;
        MouseOverUINotifier.Instance.onMouseExitUI += OnMouserExitUI;
    }

    public void SetInteractionMode(InteractionMode newMode)
    {
        _mode = newMode;
        
        onInteractionModeChanged?.Invoke(newMode);
    }

    private void OnNewActiveTool(ETools sm)
    {
        switch (sm)
        {
            case ETools.PlaceRoomTool:
                SetInteractionMode(InteractionMode.Ground);
                break;
            
            case ETools.PlaceWallTool:
                SetInteractionMode(InteractionMode.Ground);
                break;
            
            case ETools.PlaceWindowTool:
                SetInteractionMode(InteractionMode.Wall);
                break;
        }
    }
    
    private void OnMouserEnterUI()
    {
        _isOverUI = true;
    }
    
    private void OnMouserExitUI()
    {
        _isOverUI = false;
    }
}
