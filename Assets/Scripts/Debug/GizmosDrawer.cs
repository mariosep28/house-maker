using System.Collections.Generic;
using UnityEngine;

public class GizmosDrawer : Singleton<GizmosDrawer>
{
    public bool drawLineGizmo;
    public bool drawSphereGizmo;

    private struct LineGizmo
    {
        public Vector3 from;
        public Vector3 to;
        public Color color;
        
        public LineGizmo(Vector3 from, Vector3 to, Color color)
        {
            this.from = from;
            this.to = to;
            this.color = color;
        }
    }
    
    private struct SphereGizmo
    {
        public Vector3 origin;
        public float radius;
        public bool isWire;
        public Color color;
            
        public SphereGizmo(Vector3 origin, float radius, bool isWire, Color color)
        {
            this.origin = origin;
            this.radius = radius;
            this.isWire = isWire;
            this.color = color;
        }
    }

    private List<LineGizmo> _lineGizmoList = new List<LineGizmo>();
    private List<SphereGizmo> _sphereGizmoList = new List<SphereGizmo>(); 

    private void Awake()
    {
        _lineGizmoList = new List<LineGizmo>();
    }

    public void DrawLine(Vector3 from, Vector3 to, Color color)
    {
        LineGizmo lineGizmo = new LineGizmo(from, to, color);
        _lineGizmoList.Add(lineGizmo);
    }

    public void DrawWireSphere(Vector3 origin, float radius, Color color)
    {
        SphereGizmo sphereGizmo = new SphereGizmo(origin, radius, true, color);
        _sphereGizmoList.Add(sphereGizmo);
    }
    
    public void DrawSphere(Vector3 origin, float radius, Color color)
    {
        SphereGizmo sphereGizmo = new SphereGizmo(origin, radius, false, color);
        _sphereGizmoList.Add(sphereGizmo);
    }
    
    private void OnDrawGizmos()
    {
        if (drawLineGizmo)
        {
            foreach (LineGizmo lineGizmo in _lineGizmoList)
            {
                Gizmos.color = lineGizmo.color;
                Gizmos.DrawLine(lineGizmo.from, lineGizmo.to);    
            }    
        }

        if (drawSphereGizmo)
        {
            foreach (SphereGizmo sphereGizmo in _sphereGizmoList)
            {
                Gizmos.color = sphereGizmo.color;
                
                if (sphereGizmo.isWire)
                {
                    Gizmos.DrawWireSphere(sphereGizmo.origin, sphereGizmo.radius);    
                }
                else
                {
                    Gizmos.DrawSphere(sphereGizmo.origin, sphereGizmo.radius);
                }
            }
        }
        
        _lineGizmoList = new List<LineGizmo>();
        _sphereGizmoList = new List<SphereGizmo>();
    }
}
