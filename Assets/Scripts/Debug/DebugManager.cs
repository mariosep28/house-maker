using System;
using UnityEngine;

public class DebugManager : Singleton<DebugManager>
{
    [Header("Debug messages")]
    public bool commandInvoker;
    public bool stateMachine;
}
