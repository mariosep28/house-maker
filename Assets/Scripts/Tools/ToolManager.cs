using System;
using UnityEngine;

public enum ETools
{
     PlaceRoomTool,
     PlaceWallTool,
     PlaceWindowTool
}

public class ToolManager : Singleton<ToolManager>
{
     public ETools initialTool;
     
     [SerializeField] private ETools activeTool;
     private StateMachine _activeToolSM;

     public Action<ETools> onNewActiveTool;
     public ETools ActiveTool => activeTool;

     private void Start()
     {
          SetActiveTool(initialTool);
     }

     public void SetActiveTool(ETools newTool)
     {
          if (_activeToolSM != null)
          {
               _activeToolSM.Exit();
          }
          
          onNewActiveTool?.Invoke(newTool);

          activeTool = newTool;
          
          StateMachine newToolSM = ToolFactory.Create(newTool);
          newToolSM.Enter();
          
          _activeToolSM = newToolSM;
     }

     private void Update()
     {
          if(_activeToolSM != null && !InteractionManager.Instance.IsOverUI)
               _activeToolSM.Update();
     }

     private void LateUpdate()
     {
          if(_activeToolSM != null && !InteractionManager.Instance.IsOverUI)
               _activeToolSM.LateUpdate();
     }

     public bool CurrentStateIsFinal => _activeToolSM.GetCurrentState().IsFinal;
}
