using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.LowLevel;
using UnityEngine.PlayerLoop;

public class FaceSelectionTool : MonoBehaviour
{
    public bool selectionEnabled;

    private Cube cube;
    private MeshCollider _collider;
    
    
    private void Awake()
    {
        _collider = GetComponent<MeshCollider>();

        selectionEnabled = false;
    }

    private void Update()
    {
        if (selectionEnabled)
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(_collider.Raycast(ray, out var hit, 1000))
            {
                Debug.Log($"Currently pointing on {hit.triangleIndex} of the cube", this);
            }    
        }
    }
}
