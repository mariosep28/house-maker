using System;

public class GameManager : Singleton<GameManager>
{
    public Action onAccept;
    public Action onCancel;
    
    private void Update()
    {
        HandleActions();
    }

    private void HandleActions()
    {
        if(InputManager.Undo.triggered && !ToolManager.Instance.CurrentStateIsFinal)
            Undo();
        
        if(InputManager.Redo.triggered && !ToolManager.Instance.CurrentStateIsFinal)
            Redo();
        
        if(InputManager.Accept.triggered)
            Accept();
        
        if(InputManager.Cancel.triggered)
            Cancel();
    }

    private void Undo()
    {
        CommandInvoker.Instance.Undo();
    }
    
    private void Redo()
    {
        CommandInvoker.Instance.Redo();
    }

    private void Accept()
    {
        onAccept?.Invoke();
    }

    private void Cancel()
    {
        onCancel?.Invoke();
    }
}
