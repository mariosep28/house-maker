using Building.Helpers;
using PointerHandler;
using UnityEngine;

public class WallPlacer
{
    private Wall _wall;

    private PlacementChecker _placementChecker;

    private const float MinLength = 1f;
    
    public WallPlacer(Wall wall)
    {
        _wall = wall;
        
        _placementChecker = wall.gameObject.AddComponent<PlacementChecker>();
        _placementChecker.onPlaceableValueChanged += OnPlaceableValueChanged;
    }

    public void UpdateEndPosition(Vector3 endPosition)
    {
        Vector3 diff = endPosition - _wall.startPosition;
        float length = diff.magnitude;
        
        Debug.Log("Length: " + length);
        
        endPosition = ClampToMinRotationWithWallConnections(endPosition);
        
        //GizmosDrawer.Instance.DrawSphere(endPosition, 1f, Color.red);

        /*GizmosDrawer.Instance.DrawLine(_wall.startPosition + Vector3.up, endPosition + Vector3.up, Color.red);
        GizmosDrawer.Instance.DrawLine(_wall.startPosition + Vector3.up, _wall.startPosition + dir + Vector3.up, Color.red);*/
        endPosition = ClampToMinLength(endPosition);

        _wall.SetEndPosition(endPosition);
    }

    private Vector3 ClampToMinLength(Vector3 endPosition)
    {
        Vector3 diff = endPosition - _wall.startPosition;
        float length = diff.magnitude;
        
        if (length < MinLength)
        {
            Vector3 wallDir;
            if (length == 0f)
            {
                Vector3 mousePosDiff = Pointer.Instance.positionWithoutSnap - _wall.startPosition;
                wallDir = mousePosDiff.normalized;
                wallDir = SnapOperations.SnapPosition(wallDir);
            }
            else
            {
                wallDir = diff.normalized;
            }
            
            endPosition = _wall.startPosition + (wallDir * MinLength);
        }

        return endPosition;
    }
    
    private Vector3 ClampToMinRotationWithWallConnections(Vector3 endPosition)
    {
        if (_placementChecker.wallConnections.Count > 0)
        {
            Wall wallConnected = _placementChecker.wallConnections[0];

            Vector3 wallToPlaceDirection = (endPosition - _wall.startPosition).normalized; 
            Vector3 wallConnectedDirection = wallConnected.GetWallDirection();
            
            GizmosDrawer.Instance.DrawLine(wallConnected.startPosition + Vector3.up, wallConnected.startPosition + Vector3.up + wallConnectedDirection, Color.yellow);
            GizmosDrawer.Instance.DrawLine(_wall.startPosition + Vector3.up, _wall.startPosition + Vector3.up + wallToPlaceDirection, Color.red);
            
            GizmosDrawer.Instance.DrawSphere(_wall.startPosition, 1f, Color.red);
            GizmosDrawer.Instance.DrawSphere(endPosition, 1f, Color.red);
            
            float angleBetweenWalls = Vector3.SignedAngle(wallConnectedDirection, wallToPlaceDirection, Vector3.up);
            Debug.Log(angleBetweenWalls);
            
            if (Mathf.Abs(angleBetweenWalls) < 30f)
            {
                float angleSign = Mathf.Sign(angleBetweenWalls);

                Vector3 diff = endPosition - _wall.startPosition;
                float length = diff.magnitude;
                
                Vector3 minAngleDirection = Quaternion.Euler(new Vector3(0f,   angleSign * 30f, 0f)) * wallConnectedDirection;

                return _wall.startPosition + minAngleDirection * length;
            }
        }

        return endPosition;
    }

    public bool IsPlaceable()
    {
        return _placementChecker.Placeable;
    }
    
    private void OnPlaceableValueChanged(bool placeable)
    {
        if(placeable)
            _wall.ChangeMaterial(DefaultDataManager.Instance.PlaceableMaterial);
        else
            _wall.ChangeMaterial(DefaultDataManager.Instance.NotPlaceableMaterial);
    }
}
