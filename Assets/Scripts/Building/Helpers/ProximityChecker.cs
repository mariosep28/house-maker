﻿using UnityEngine;

namespace Building.Helpers
{
    public class ProximityHit
    {
        public Collider collider;
        public Vector3 closestPoint;
    }
    
    public static class ProximityChecker
    {
        public static bool IsCloseToGameObject(Vector3 position, float radius, out ProximityHit proximityHit, LayerMask layerMask, GameObject gameObjectToIgnore = null)
        {
           // GizmosDrawer.Instance.DrawWireSphere(position, radius, Color.yellow);

            int maxColliders = 5;
            Collider[] hitColliders = new Collider[maxColliders];
            var size = Physics.OverlapSphereNonAlloc(position, 3f, hitColliders, layerMask);

            proximityHit = new ProximityHit();
            
            for (int i = 0; i < size; i++)
            {
                if (hitColliders[i].gameObject != gameObjectToIgnore)
                {
                    //Debug.Log("close", hitColliders[i].gameObject);

                    proximityHit.collider = hitColliders[i];
                    proximityHit.closestPoint = hitColliders[i].ClosestPoint(position);
                  //  GizmosDrawer.Instance.DrawSphere(proximityHit.closestPoint, 0.5f, Color.red);

                    return true;
                }
            }
            
            return false;
        }
    }
}