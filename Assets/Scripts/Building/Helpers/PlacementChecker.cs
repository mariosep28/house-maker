﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Building.Helpers
{
    public class PlacementChecker : MonoBehaviour
    {
        private bool _placeable;
        private bool _previousPlaceable;

        private Wall _wall;
        public List<Wall> wallConnections;
        
        private Rigidbody _rb;

        public bool Placeable => _placeable;
        
        private List<GameObject> _colliders = new List<GameObject>();
        
        public Action<bool> onPlaceableValueChanged;

        private void Awake()
        {
            _placeable = true;
            _previousPlaceable = true;

            _wall = GetComponent<Wall>();
            wallConnections = new List<Wall>();
            
            CheckWallConnections();
            
            _rb = gameObject.AddComponent<Rigidbody>();
            _rb.constraints = RigidbodyConstraints.FreezeAll;
        }
        
        private void Update()
        {
            CheckWallConnections();
            
            _placeable = CheckIfIsPlaceable();

            if (_previousPlaceable != _placeable)
                onPlaceableValueChanged?.Invoke(_placeable);
        
            _previousPlaceable = _placeable;
        }

        private bool CheckIfIsPlaceable()
        {
            int count = _colliders.Count;
            
            foreach (var wallConnected in wallConnections)
            {
                if (_colliders.Contains(wallConnected.gameObject))
                    count--;
            }

            return count == 0;
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("Trigger enter", other.gameObject);
            _colliders.Add(other.gameObject);
        }

        private void OnTriggerExit(Collider other)
        {
            Debug.Log("Trigger exit", other.gameObject);
            _colliders.Remove(other.gameObject);
        }
        
        private void CheckWallConnections()
        {
            wallConnections.Clear();
        
            ProximityHit proximityHit;
        
            if (ProximityChecker.IsCloseToGameObject(_wall.startPosition, 2f, out proximityHit, LayerMask.GetMask("Wall")))
            {
                Wall wallConnected = proximityHit.collider.GetComponent<Wall>();
            
                if(!wallConnections.Contains(wallConnected))
                    wallConnections.Add(wallConnected);
            }
        
            if (ProximityChecker.IsCloseToGameObject(_wall.endPosition, 2f, out proximityHit, LayerMask.GetMask("Wall")))
            {
                Wall wallConnected = proximityHit.collider.GetComponent<Wall>();
            
                if(!wallConnections.Contains(wallConnected))
                    wallConnections.Add(wallConnected);
            }
        }
    }
}