using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Room : MonoBehaviour
{
    public List<Wall> roomWalls;

    private void Awake()
    {
        roomWalls = new List<Wall>();
    }

    public void AddWall(Wall wall)
    {
        roomWalls.Add(wall);
        
        wall.transform.SetParent(this.transform);
    }

    public void RemoveWall(Wall wall)
    {
        roomWalls.Remove(wall);
        
        wall.transform.SetParent(null);
    }

    public Wall GetLastWall()
    {
        if (roomWalls.Count > 0)
            return roomWalls.Last();
        else
            return null;
    }

    public Wall GetFirstWall()
    {
        if (roomWalls.Count > 0)
            return roomWalls.First();
        else
            return null;
    }
}
