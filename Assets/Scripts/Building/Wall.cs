using UnityEngine;

[RequireComponent(typeof(Rectangle))]
public class Wall : MonoBehaviour
{
    private Rectangle _rectangle;

    public Vector3 startPosition;
    public Vector3 endPosition;

    public float width;
    public float height;

    public Material material;
    
    private void Awake()
    {
        _rectangle = GetComponent<Rectangle>();

        gameObject.layer = LayerMask.NameToLayer("Wall");
        gameObject.tag = "Wall";
    }

    public void Init(float width, float height, Vector3 startPosition, Vector3 endPosition, Material material)
    {
        this.width = width;
        this.height = height;

        this.startPosition = startPosition;
        this.endPosition = endPosition;

        Vector3 targetDir = endPosition - startPosition;

        if (targetDir != Vector3.zero)
        {
            Vector3 targetRotation = Quaternion.LookRotation(targetDir, Vector3.up).eulerAngles - new Vector3(0, 90f, 0);

            transform.eulerAngles = targetRotation;    
        }
        
        this.material = material;
        
        transform.position = startPosition;

        UpdateMesh();
    }

    public void SetStartPosition(Vector3 startPosition)
    {
        this.startPosition = startPosition;
        
        UpdateMesh();
    }
    
    public void SetEndPosition(Vector3 endPosition)
    {
        this.endPosition = endPosition;

        UpdateMesh();

        Vector3 targetDir = endPosition - startPosition;

        if (targetDir != Vector3.zero)
        {
            Vector3 targetRotation = Quaternion.LookRotation(targetDir, Vector3.up).eulerAngles - new Vector3(0, 90f, 0);

            transform.eulerAngles = targetRotation;    
        }
    }
    
    private void UpdateMesh()
    {
        _rectangle.Init(width, height, startPosition, endPosition, material);
    }

    public Vector3 GetWallDirection()
    {
        return (endPosition - startPosition).normalized;
    }
    
    public void ChangeMaterial(Material material)
    {
        this.material = material;
        _rectangle.ChangeMaterial(material);
    }
}
