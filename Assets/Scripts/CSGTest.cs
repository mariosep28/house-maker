using System;
using System.Collections;
using System.Collections.Generic;
using Parabox.CSG;
using UnityEngine;
using UnityEngine.Serialization;

public class CSGTest : MonoBehaviour
{
    public GameObject wall;
    public GameObject window;

    private void Start()
    {
        /*wall = transform.GetChild(0).gameObject;
        cube = transform.GetChild(1).gameObject;*/
    }

    [ContextMenu("Subtract")]
    public void Subtract()
    {
        MeshFilter meshFilter = wall.GetComponent<MeshFilter>(); 
        
        //meshFilter.sharedMesh = CSG.Subtract(wall, cube).mesh;
        
        Model result = CSG.Subtract(wall, window);

        meshFilter.sharedMesh = result.mesh;
        
        window.SetActive(false);
        
        /*GameObject composite = new GameObject();
        
        composite.AddComponent<MeshFilter>().sharedMesh = result.mesh;
        composite.AddComponent<MeshRenderer>().sharedMaterials = result.materials.ToArray();*/

        //GenerateBarycentric(composite);

        /*Destroy(left);
        Destroy(right);*/
        
    }
}
