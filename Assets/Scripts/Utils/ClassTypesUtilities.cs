using System;
using System.Collections.Generic;
using System.Linq;

public static class ClassTypesUtilities
{
    public static IEnumerable<Type> FindSubClassesOf<TBaseType>()
    {   
        var baseType = typeof(TBaseType);
        var assembly = baseType.Assembly;

        return assembly.GetTypes().Where(t => t.IsSubclassOf(baseType));
    } 
    
    public static T ConvertObject<T>(object input) {
        return (T) Convert.ChangeType(input, typeof(T));
    }

}
