using UnityEngine;

public class DefaultDataManager : Singleton<DefaultDataManager>
{
    public DefaultData defaultData;
    
    public Material DefaultMaterial => defaultData.defaultMaterial;
    public Material PlaceableMaterial => defaultData.placeableMaterial;
    public Material NotPlaceableMaterial => defaultData.notPlaceableMaterial;

    public GameObject WindowPrimitive => defaultData.windowPrimitive;
    
    public float WallWidth => defaultData.wallWidth;
    public float WallHeight => defaultData.wallHeight;
}

