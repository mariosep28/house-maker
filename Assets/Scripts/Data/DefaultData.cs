using UnityEngine;

[CreateAssetMenu]
public class DefaultData : ScriptableObject
{
    public Material defaultMaterial;
    public Material placeableMaterial;
    public Material notPlaceableMaterial;

    [Header("Build primitives")] public GameObject windowPrimitive;
    
    [Header("Wall settings")] 
    public float wallWidth;
    public float wallHeight;
}
