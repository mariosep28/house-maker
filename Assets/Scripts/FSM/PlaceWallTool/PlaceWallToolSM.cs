﻿using FSM.PlaceWallTool.States;

namespace FSM.PlaceWallTool
{
    public class PlaceWallToolSM : StateMachine
    {
        protected override BaseState GetInitialState()
        {
            return new SelectStartWallPositionState(this, null);
        }
    }
}