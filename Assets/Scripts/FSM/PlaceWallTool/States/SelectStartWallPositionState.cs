﻿using PointerHandler;
using UnityEngine;

namespace FSM.PlaceWallTool.States
{
    public class SelectStartWallPositionState: BaseState
    {
        private PlaceWallToolSM _sm;
        
        public SelectStartWallPositionState(PlaceWallToolSM stateMachine, BaseState previousState) : base("Select start wall position", stateMachine, previousState)
        {
            _sm = stateMachine;
            isInitial = true;
        }

        public override void UpdateLogic()
        {
            base.UpdateLogic();

            if (InputManager.PrincipalAction.triggered)
            {
                Vector3 startPosition = Pointer.Instance.position;
                
                stateMachine.ChangeState(new SelectEndWallPositionState(_sm, this, startPosition));
            }
        }
    }
}