﻿using PointerHandler;
using UnityEngine;

namespace FSM.PlaceWallTool.States
{
    public class SelectEndWallPositionState : BaseState
    {
        private PlaceWallToolSM _sm;
        private Vector3 _startPosition;

        private Wall _referenceWall;
        private WallPlacer _wallPlacer;

        private AddWallCommand _addReferenceWallCommand;
        
        public SelectEndWallPositionState(PlaceWallToolSM stateMachine, BaseState previousState, Vector3 startPosition) : base("Select end wall position", stateMachine, previousState)
        {
            _sm = stateMachine;
            _startPosition = startPosition;

            isFinal = true;
        }

        public override void Enter()
        {
            base.Enter();
            
            _addReferenceWallCommand = new AddWallCommand(_startPosition, _startPosition, DefaultDataManager.Instance.PlaceableMaterial);
            _addReferenceWallCommand.Execute();

            _referenceWall = _addReferenceWallCommand.wall;
            _referenceWall.gameObject.layer = LayerMask.NameToLayer("Ignore");
            
            _wallPlacer = new WallPlacer(_referenceWall);
        }

        public override void UpdateLogic()
        {
            base.UpdateLogic();
            
            Vector3 endPosition =  Pointer.Instance.position;
            
            _wallPlacer.UpdateEndPosition(endPosition);
            
            if (InputManager.PrincipalAction.triggered && _wallPlacer.IsPlaceable())
            {
                AddWallCommand addWallCommand = new AddWallCommand(_startPosition, endPosition, DefaultDataManager.Instance.DefaultMaterial);
                CommandInvoker.Instance.AddCommand(addWallCommand);

                stateMachine.ChangeState(new SelectStartWallPositionState(_sm, this));    
            }
        }
        
        public override void Exit()
        {
            base.Exit();
            
            Object.Destroy(_referenceWall.gameObject);
        }
    }
}