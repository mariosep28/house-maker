using UnityEngine;
using Parabox.CSG;

public class PlaceWindowState : BaseState
{
    private WindowToolSM _sm;

    private Wall _wallFocused;

    private GameObject _windowGameObject; 
    private Vector3 _windowPosition;
    private Quaternion _windowRotation;
    
    public PlaceWindowState(WindowToolSM stateMachine, BaseState previousState) : base("Place window", stateMachine, previousState)
    {
        _sm = stateMachine;
        isInitial = true;
    }

    public override void Enter()
    {
        base.Enter();
        
        MousePosition.Instance.onStartToPoint += OnStartToPointWall;
        MousePosition.Instance.onEndToPoint += OnEndToPointWall;
        
        AddWindowCommand addWindowCommand = new AddWindowCommand(_windowPosition, Quaternion.identity, false);
        addWindowCommand.Execute();
        
        _windowGameObject = addWindowCommand.windowGo;
        
        GameObject objectPointed = MousePosition.Instance.ObjectPointed;

        if (objectPointed != null && objectPointed.CompareTag("Wall"))
        {
            OnStartToPointWall(objectPointed);
        }
    }

    public override void UpdateLogic()
    {
        base.UpdateLogic();

        if (_wallFocused != null)
        {
            PlaceWindow();
            
            if (InputManager.PrincipalAction.triggered)
            {
                AddWindowCommand addWindowCommand = new AddWindowCommand(_windowPosition, _windowRotation);
                CommandInvoker.Instance.AddCommand(addWindowCommand);
                
                stateMachine.ChangeState(new PlaceWindowState(_sm, this));
            }
        }
    }

    public override void Exit()
    {
        base.Exit();
        
        MousePosition.Instance.onStartToPoint -= OnStartToPointWall;
        MousePosition.Instance.onEndToPoint -= OnEndToPointWall;
        
        Object.Destroy(_windowGameObject);
    }

    private void PlaceWindow()
    {
        _windowPosition = MousePosition.Instance.worldPosition;
        _windowPosition = SnapOperations.SnapToMiddleObjectAxis(_windowPosition, _wallFocused.gameObject, SnapAxis.Y);
        
        _windowRotation = Quaternion.LookRotation(MousePosition.Instance.MouseHitNormal);
        
        _windowGameObject.transform.position = _windowPosition;
        _windowGameObject.transform.rotation = _windowRotation;
        
        /*_wallMeshFilter.sharedMesh = _originalWallMesh;
        _wallMeshFilter.mesh = CSG.Subtract(_wallFocused.gameObject, _windowGameObject).mesh;*/
    }
    
    private void OnStartToPointWall(GameObject wallPointedGo)
    {
        _wallFocused = wallPointedGo.GetComponent<Wall>();
        Debug.Log("New wall focused", _wallFocused.gameObject);
        
        _windowGameObject.SetActive(true);
    }

    private void OnEndToPointWall(GameObject wallPointedGo)
    {
        _wallFocused = null;
        
        _windowGameObject.SetActive(false);
    }
}
