public class WindowToolSM : StateMachine
{
    protected override BaseState GetInitialState()
    {
        return new PlaceWindowState(this, null);
    }
}
