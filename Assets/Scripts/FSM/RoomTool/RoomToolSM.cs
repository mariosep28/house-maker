public class RoomToolSM : StateMachine
{
    public Room room;

    protected override BaseState GetInitialState()
    {
        return new SelectFirstWallStartPositionState(this, null);
    }
}
