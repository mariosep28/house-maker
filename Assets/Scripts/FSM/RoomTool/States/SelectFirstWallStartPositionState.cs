using PointerHandler;
using UnityEngine;

public class SelectFirstWallStartPositionState : BaseState
{
    private RoomToolSM _sm;
    
    public SelectFirstWallStartPositionState(RoomToolSM stateMachine, BaseState previousState) : base("Select first wall position", stateMachine, previousState)
    {
        _sm = stateMachine;
        isInitial = true;
    }
    
    public override void UpdateLogic()
    {
        base.UpdateLogic();

        if (InputManager.PrincipalAction.triggered)
        {
            ICommand command = new CreateRoomCommand(_sm); 
            CommandInvoker.Instance.AddCommand(command);

            Vector3 startPosition = Pointer.Instance.position;
            
            stateMachine.ChangeState(new SelectWallEndPositionState(_sm, this, startPosition));
        }
    }
}
