using UnityEngine;

public class SelectWallEndPositionState : BaseState
{
    private RoomToolSM _sm;
    private Wall _referenceWall;
    private Vector3 _startPosition;
    private WallPlacer _wallPlacer;

    private AddWallCommand addReferenceWallCommand;
    
    private Vector3 _movementVelocity = new Vector3();
    private float _timeToReachTarget = 0.005f;
    
    public SelectWallEndPositionState(RoomToolSM stateMachine, BaseState previousState, Vector3 startPosition) : base("Select wall end position", stateMachine, previousState)
    {
        _sm = stateMachine;
        _startPosition = startPosition;
    }

    public override void Enter()
    {
        base.Enter();
        
        addReferenceWallCommand = new AddWallCommand(_startPosition, _startPosition, DefaultDataManager.Instance.PlaceableMaterial);
        addReferenceWallCommand.Execute();

        _referenceWall = addReferenceWallCommand.wall;

        _wallPlacer = new WallPlacer(_referenceWall);
    }

    public override void UpdateLogic()
    {
        base.UpdateLogic();

        Vector3 endPosition = MousePosition.Instance.worldPosition;
        
        // TODO: Extract snap and smooth logic
        
        // Snap when close to straight angles        
        endPosition = SnapOperations.SnapWhenCloseToStraightAngles(_startPosition, endPosition);
        // Smooth position        
        endPosition = Vector3.SmoothDamp(_referenceWall.endPosition, endPosition, ref _movementVelocity, _timeToReachTarget);
        
        if (_referenceWall == null)
            _referenceWall = addReferenceWallCommand.wall;

        if (_referenceWall != null)
        {
            _wallPlacer.UpdateEndPosition(endPosition);
        }

        if (InputManager.PrincipalAction.triggered && _wallPlacer.IsPlaceable())
        {
            AddWallCommand addWallCommand = new AddWallCommand(_startPosition, _startPosition, DefaultDataManager.Instance.DefaultMaterial);
            CommandInvoker.Instance.AddCommand(addWallCommand);

            ICommand setNextWallPositionCommand = new SetNextWallPositionCommand(addWallCommand.wall, _referenceWall.endPosition);
            CommandInvoker.Instance.AddCommand(setNextWallPositionCommand);

            /*if (_wallPlacer.isCloseToWall)
            {
                stateMachine.ChangeState(new CompleteRoomState(_sm, this));
            }
            else
            {*/
                stateMachine.ChangeState(new SelectWallEndPositionState(_sm, this, _referenceWall.endPosition));    
            //}
        }
    }

    public override void Exit()
    {
        base.Exit();
        
        Object.Destroy(_referenceWall.gameObject);
    }
}
