using UnityEngine;

public class CompleteRoomState : BaseState
{
    private RoomToolSM _sm;
    
    public CompleteRoomState(RoomToolSM stateMachine, BaseState previousState) : base("Complete room", stateMachine, previousState)
    {
        _sm = stateMachine;
        isFinal = true;
    }

    public override void Enter()
    {
        base.Enter();

        /*ICommand command = new CompleteRoomCommand(_sm.room);
        CommandInvoker.Instance.AddCommand(command, this);*/
        
        stateMachine.ChangeState(new SelectFirstWallStartPositionState(_sm, this));
    }
}
