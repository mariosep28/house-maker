using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class StateMachine
{
    public ETools tool;
    
    private List<BaseState> _completedStatesHistory;
    private BaseState _currentState;

    public StateMachine()
    {
        CommandInvoker.Instance.onUndo += RestartSM;
        GameManager.Instance.onCancel += RestartSM;
    }
    
    public void Enter()
    {
        _completedStatesHistory = new List<BaseState>();
        
        BaseState initialState = GetInitialState();
        
        if(initialState != null)
            ChangeState(initialState);
    }
    
    public void Update()
    {
        if (_currentState != null)
            _currentState.UpdateLogic();
    }

    public void LateUpdate()
    {
        if (_currentState != null)
            _currentState.UpdatePhysics();
    }

    public void Exit()
    {
        _currentState.Exit();
    }

    public void ChangeState(BaseState newState)
    {
        string message = "RoomToolSM: "; 
        
        if (_currentState != null)
        {
            _currentState.Exit();
            _currentState.NextState = newState;
            
            _completedStatesHistory.Add(_currentState);
            
           message = string.Concat(message, $"{_currentState.name} -> ");
        }
        message = string.Concat(message, $"{newState.name}");
        
        if(DebugManager.Instance.stateMachine)
            Debug.Log(message);
        
        _currentState = newState;
        _currentState.Enter();
    }

    public void ReturnToPreviousState()
    {
        _currentState.Exit();
        
        BaseState previousState = _completedStatesHistory.Last();
        
        do
        {
            _currentState = previousState;
            
            _completedStatesHistory.Remove(previousState);
            
            if(_completedStatesHistory.Count == 0)
                break;
                
            previousState = _completedStatesHistory.Last();    
            
        } while(!previousState.IsFinal);
        
        _currentState.Enter();
    }

    public void RestartSM()
    {
        if (!_currentState.IsInitial)
        {
            if(_currentState != null)
                _currentState.Exit();

            _currentState = GetInitialState();
            _currentState.Enter();    
        }
    }
    
    protected abstract BaseState GetInitialState();

    public BaseState GetCurrentState() => _currentState;
    
    public BaseState GetPreviousState() => _completedStatesHistory.Last();
    
    private void OnGUI()
    {
        string content = _currentState != null ? _currentState.name : "(no current state)";
        GUILayout.Label($"<color='black'><size=40>{content}</size></color>");
    }
}
