using System;

public abstract class BaseState
{
    public string name;
    protected StateMachine stateMachine;
    protected BaseState previousState;
    protected BaseState nextState;
    protected bool isFinal;
    protected bool isInitial;
    
    public BaseState(string name, StateMachine stateMachine, BaseState previousState)
    {
        this.name = name;
        this.stateMachine = stateMachine;
        this.previousState = previousState;
    }

    public StateMachine GetStateMachine => stateMachine;

    public BaseState GetPreviousState => previousState;

    public bool IsFinal => isFinal;
    public bool IsInitial => isInitial;

    public BaseState NextState
    {
        get { return nextState; }
        set { nextState = value; }
    }
    
    public virtual void Enter() {}
    public virtual void UpdateLogic() {}
    public virtual void UpdatePhysics() {}
    public virtual void Exit() {}
}
