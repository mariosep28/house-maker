using FSM.PlaceWallTool;

public static class ToolFactory
{
    public static StateMachine Create(ETools tool)
    {
        switch (tool)
        {
            case ETools.PlaceRoomTool:
                return new RoomToolSM();
            
            case ETools.PlaceWallTool:
                return new PlaceWallToolSM();
            
            case ETools.PlaceWindowTool:
                return new WindowToolSM();
            
            default:
                return null;
        }
    }
}
