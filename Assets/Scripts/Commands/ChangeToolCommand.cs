﻿public class ChangeToolCommand : ICommand
{
    private ETools newTool;
    private ETools previousTool;
    
    public ChangeToolCommand(ETools newTool)
    {
        this.newTool = newTool;
        this.previousTool = ToolManager.Instance.ActiveTool;
    }
    
    public void Execute()
    {
        ToolManager.Instance.SetActiveTool(newTool);
    }

    public void Undo()
    {
        ToolManager.Instance.SetActiveTool(previousTool);
    }
}
