using UnityEngine;

public class CompleteRoomCommand : ICommand
{
    public Room room;

    public CompleteRoomCommand(Room room)
    {
        this.room = room;
    }

    public void Execute()
    {
        MeshCombiner.CombineChildMesh(room.gameObject);
    }

    public void Undo()
    {
        
    }
}
