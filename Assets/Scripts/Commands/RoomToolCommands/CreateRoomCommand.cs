using UnityEngine;

public class CreateRoomCommand : ICommand
{
    public RoomToolSM roomToolSm;
    public Room room;

    public CreateRoomCommand(RoomToolSM roomToolSm)
    {
        this.roomToolSm = roomToolSm;
    }
    
    public void Execute()
    {
        // TODO: Extract logic to RoomFactory.
        room = new GameObject("Room", typeof(Room)).GetComponent<Room>();

        roomToolSm.room = room;
    }

    public void Undo()
    {
        if (room != null)
        {
            roomToolSm.room = null;
            
            Object.Destroy(room.gameObject);
        }
    }
}
