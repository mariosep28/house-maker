using UnityEngine;

public class SetNextWallPositionCommand : ICommand
{
    private RoomToolSM _sm;
    
    public Wall wall;

    public Vector3 endPosition;

    // TODO: Choose another class name
    public SetNextWallPositionCommand(Wall wall, Vector3 endPosition)
    {
        //this._sm = sm;
        this.wall = wall;
        this.endPosition = endPosition;
    }

    public void Execute()
    {
        /*if(wall == null)
            wall = _sm.room.GetLastWall();*/
        
        wall.SetEndPosition(endPosition);        
    }

    public void Undo()
    {
        
    }
}
