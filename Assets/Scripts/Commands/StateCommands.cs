using System.Collections.Generic;

public class StateCommands
{
    private BaseState _state;
    private List<ICommand> _commands;

    public BaseState State => _state;
    public List<ICommand> Commands => _commands;
    
    public StateCommands(BaseState state)
    {
        _state = state;
        _commands = new List<ICommand>();
    }

    public void AddCommand(ICommand command)
    {
        _commands.Add(command);
    }
}
