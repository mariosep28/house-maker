using System;
using System.Collections.Generic;
using UnityEngine;

public class CommandInvoker : Singleton<CommandInvoker>
{
    private Stack<ICommand> _commandsStack;
    private Stack<ICommand> _redoCommandStack;

    public Action onUndo;
    public Action onRedo;

    private void Awake()
    {
        _commandsStack = new Stack<ICommand>();
        _redoCommandStack = new Stack<ICommand>();
    }
    
    public void AddCommand(ICommand command)
    {
        _commandsStack.Push(command);
        command.Execute();

        if(DebugManager.Instance.commandInvoker)
            Debug.Log($"Executed: {command.ToString()}.");

        if (_redoCommandStack.Count > 0)
            _redoCommandStack = new Stack<ICommand>();
    }
    
    public void Undo()
    {
        if (_commandsStack.Count > 0)
        {
            ICommand commandToUndo = _commandsStack.Pop();
            commandToUndo.Undo();
            _redoCommandStack.Push(commandToUndo);
            
            if(DebugManager.Instance.commandInvoker)
                Debug.Log($"Undone: {commandToUndo.ToString()}.");
            
            onUndo?.Invoke();
        }
    }

    public void Redo()
    {
        if (_redoCommandStack.Count > 0)
        {
            ICommand commandToRedo = _redoCommandStack.Pop();
            _commandsStack.Push(commandToRedo);
            commandToRedo.Execute();
            
            if(DebugManager.Instance.commandInvoker)
                Debug.Log($"Redone: {commandToRedo.ToString()}.");

            onRedo?.Invoke();
        }
    }
}
