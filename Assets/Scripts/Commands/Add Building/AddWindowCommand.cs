using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddWindowCommand : ICommand
{
    public Vector3 windowPosition;
    public Quaternion windowRotation;
    
    public GameObject windowGo;
    public bool activeByDefault;

    public AddWindowCommand(Vector3 windowPosition, Quaternion windowRotation, bool activeByDefault = true)
    {
        this.windowPosition = windowPosition;
        this.windowRotation = windowRotation;
        this.activeByDefault = activeByDefault;
    }
    
    public void Execute()
    {
        windowGo = GameObject.CreatePrimitive(PrimitiveType.Cube);
        windowGo.SetActive(activeByDefault);
        windowGo.transform.position = windowPosition;
        windowGo.transform.rotation = windowRotation;
    }

    public void Undo()
    {
        Object.Destroy(windowGo.gameObject);
    }
}
