using UnityEngine;

public class AddWallCommand : ICommand
{
    public Wall wall;

    public float width;
    public float height;
    public Vector3 startPosition;
    public Vector3 endPosition;
    public Material material;

    public AddWallCommand(Vector3 startPosition, Vector3 endPosition, Material material)
    {
        this.width = DefaultDataManager.Instance.WallWidth;
        this.height = DefaultDataManager.Instance.WallHeight;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.material = material;
    }
    
    public AddWallCommand(float width, float height, Vector3 startPosition, Vector3 endPosition, Material material)
    {
        this.width = width;
        this.height = height;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.material = material;
    }
    
    public void Execute()
    {
        // TODO: Extract logic to WallFactory.
        wall = new GameObject("Wall", typeof(Wall)).GetComponent<Wall>();
        
        wall.Init(width, height, startPosition, endPosition, material);
    }

    public void Undo()
    {
        Object.Destroy(wall.gameObject);
    }
}
