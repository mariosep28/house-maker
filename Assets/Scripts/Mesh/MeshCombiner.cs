using UnityEngine;

public class MeshCombiner
{
    [ContextMenu("Combine child mesh")]
    public static void CombineChildMesh(GameObject combineGameObject)
    {
        MeshFilter[] meshFilters = combineGameObject.GetComponentsInChildren<MeshFilter>();
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];

        int i = 0;
        while (i < meshFilters.Length)
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.SetActive(false);

            i++;
        }

        MeshFilter meshFilter = combineGameObject.GetComponent<MeshFilter>();
        MeshRenderer meshRenderer = combineGameObject.GetComponent<MeshRenderer>();

        meshFilter.mesh = new Mesh();
        meshFilter.mesh.CombineMeshes(combine);

        meshRenderer.material = DefaultDataManager.Instance.DefaultMaterial;
        
        combineGameObject.SetActive(true);
    }
}
