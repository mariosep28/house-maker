using System.Collections.Generic;
using UnityEngine;

public class Shape
{
    public List<Vector3> vertices;
    public List<int> triangles;
    public List<Vector3> normals;
    public List<Vector2> uv;
    public List<Vector2> colors;

    private int _vertexOffset = 0;
    
    public Shape()
    {
        vertices = new List<Vector3>();
        triangles = new List<int>();
        normals = new List<Vector3>();
        uv = new List<Vector2>();
        colors = new List<Vector2>();
    }

    public void CreateQuad(Vector3 pos, Vector3 widthDir, Vector3 lengthDir, Vector3 offset)
    {
        // vertex indices
        int i1 = _vertexOffset;
        int i2 = _vertexOffset + 1;
        int i3 = _vertexOffset + 2;
        int i4 = _vertexOffset + 3;
        
        // vertices
        vertices.Add(pos - widthDir + lengthDir + offset);
        vertices.Add(pos + widthDir + lengthDir + offset);
        vertices.Add(pos + widthDir - lengthDir + offset);
        vertices.Add(pos - widthDir - lengthDir + offset);
        
        _vertexOffset += 4;
        
        // triangles
        triangles.AddRange(new int[]{ i1, i2, i3, i1, i3, i4 });

        // normals
        Vector3 normal = GetSurfaceNormal(vertices[i1], vertices[i2], vertices[i3]);
        for (int i = 0; i < 4; i++)
        {
            normals.Add(normal);    
        }
        
        // UVs
        uv.Add(new Vector2(0, 1)); // 0
        uv.Add(new Vector2(1, 1)); // 1
        uv.Add(new Vector2(1, 0)); // 2
        uv.Add(new Vector2(0, 0)); // 3 
    }
    
    private Vector3 GetSurfaceNormal (Vector3 v1, Vector3 v2, Vector3 v3) 
    {
        // get edge directions
        Vector3 edge1 = v2 - v1;
        Vector3 edge2 = v3 - v2;
        // get normalized cross product
        Vector3 normal = Vector3.Cross(edge1, edge2).normalized;
        
        return normal;
    }

    public List<int> GetFaceVertices(int triangleIndex)
    {
        List<int> faceVertices = new List<int>();

        int faceNum = triangleIndex / 3;

        int faceVertex1 = faceNum * 3;
        int faceVertex2 = faceNum * 3 + 1;
        int faceVertex3 = faceNum * 3 + 2;
        
        faceVertices.Add(faceVertex1);
        faceVertices.Add(faceVertex2);
        faceVertices.Add(faceVertex3);

        return faceVertices;
    }

    public void ChangeVertexColor(List<int> vertices)
    {
        
    }
}
