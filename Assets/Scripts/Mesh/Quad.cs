using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class Quad : MonoBehaviour
{
    [Range(1, 5)] public float width = 1;
    [Range(1, 5)] public float length = 1;
    
    public Vector3 offset = new Vector3(0, 0, 0);

    private Shape _shape;
    
    private MeshRenderer _meshRenderer;
    private MeshFilter _meshFilter;
    
    public Vector3 StartPosition => transform.position;
    
    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _meshFilter = GetComponent<MeshFilter>();
        
        HideMeshComponents();
        
        UpdateMesh();
    }
    
    private void OnValidate()
    {
        if (_meshRenderer == null || _meshFilter == null)
        {
            _meshRenderer = GetComponent<MeshRenderer>();
            _meshFilter = GetComponent<MeshFilter>();
        
            HideMeshComponents();    
        }
        
        UpdateMesh();
    }

    public void SetSize(float width, float length)
    {
        this.width = width;
        this.length = length;
        
        UpdateMesh();
    }
    
    public void SetOffset(Vector3 offset)
    {
        this.offset = offset;
    }
    
    public void GenerateMesh()
    {
        float halfWidth = width / 2;
        float halfLength = length / 2;
        
        _shape = new Shape();
        
        // Create quad
        _shape.CreateQuad(new Vector3(0, 0, 0), new Vector3(halfWidth, 0, 0), new Vector3(0, 0, halfLength), offset);
        
        Mesh mesh = new Mesh();

        mesh.vertices = _shape.vertices.ToArray();
        mesh.triangles = _shape.triangles.ToArray();
        mesh.normals = _shape.normals.ToArray();

        _meshFilter.mesh = mesh;
        _meshRenderer.material = DefaultDataManager.Instance.DefaultMaterial;
    }
    
    public void UpdateMesh()
    {
        GenerateMesh();
    }

    private void HideMeshComponents()
    {
        _meshRenderer.hideFlags = HideFlags.HideInInspector;
        _meshFilter.hideFlags = HideFlags.HideInInspector;
    }
}
