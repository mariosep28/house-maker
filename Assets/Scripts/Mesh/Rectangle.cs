using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter), typeof(BoxCollider))]
public class Rectangle : MonoBehaviour
{
    public float width = 1;
    public float height = 1;
    private float _length; 
        
    public Vector3 offset = new Vector3(0, 0, 0);

    public Vector3 startPosition;
    public Vector3 endPosition;

    public Material material;
    
    private Shape _shape;
    
    private MeshRenderer _meshRenderer;
    private MeshFilter _meshFilter;
    private BoxCollider _boxCollider;

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _meshFilter = GetComponent<MeshFilter>();
        _boxCollider = GetComponent<BoxCollider>();
        
        HideMeshComponents();
        
        UpdateMesh();
    }
    
    /*private void OnValidate()
    {
        if (_meshRenderer == null || _meshFilter == null)
        {
            _meshRenderer = GetComponent<MeshRenderer>();
            _meshFilter = GetComponent<MeshFilter>();
            _meshCollider = GetComponent<MeshCollider>();
        
            HideMeshComponents();    
        }
        
        UpdateMesh();
    }*/
    
    public void Init(float width, float height, Vector3 startPosition, Vector3 endPosition, Material material)
    {
        this.width = width;
        this.height = height;
        
        this.startPosition = startPosition;
        this.endPosition = endPosition;

        this.material = material;
        
        _length = (endPosition - startPosition).x;

        CalculateOffset();
        
        UpdateMesh();
    }

    public void GenerateMesh()
    {
        _length = (endPosition - startPosition).magnitude;
        
        CalculateOffset();
        
        float halfHeight = height / 2;
        float halfWidth = width / 2;
        float halfLength = _length / 2;
        
        _shape = new Shape();
        
        // Create quads
        // top
        _shape.CreateQuad(new Vector3(0, halfHeight, 0), new Vector3(halfLength, 0, 0), new Vector3(0, 0, halfWidth), offset);
        // bottom
        _shape.CreateQuad(new Vector3(0, -halfHeight, 0), new Vector3(-halfLength, 0, 0), new Vector3(0, 0, halfWidth), offset);
        // sides
        _shape.CreateQuad(new Vector3(-halfLength, 0, 0), new Vector3(0, 0, -halfWidth), new Vector3(0, halfHeight, 0), offset); // left
        _shape.CreateQuad(new Vector3(halfLength, 0, 0), new Vector3(0, 0, halfWidth), new Vector3(0, halfHeight, 0), offset); // right
        _shape.CreateQuad(new Vector3(0, 0, -halfWidth), new Vector3(halfLength, 0, 0), new Vector3(0, halfHeight, 0), offset); // front
        _shape.CreateQuad(new Vector3(0, 0, halfWidth), new Vector3(-halfLength, 0, 0), new Vector3(0, halfHeight, 0), offset); // back
        
        Mesh mesh = new Mesh();
        
        mesh.vertices = _shape.vertices.ToArray();
        mesh.triangles = _shape.triangles.ToArray();
        mesh.normals = _shape.normals.ToArray();
        
        _meshFilter.mesh = mesh;
        _meshRenderer.material = material;

        Bounds meshBounds = _meshFilter.mesh.bounds;

        _boxCollider.center = meshBounds.center;
        _boxCollider.size = meshBounds.size;
        _boxCollider.isTrigger = true;
        
        transform.position = Vector3.Lerp(startPosition, endPosition, 0.5f);
    }
    
    [ContextMenu("UpdateMesh")]
    public void UpdateMesh()
    {
        GenerateMesh();
    }
    
    public void ChangeMaterial(Material material)
    {
        _meshRenderer.sharedMaterial = material;
    }

    private void CalculateOffset()
    {
        offset = Vector3.up * height / 2.0f; // (Vector3.right * _length / 2.0f) + (Vector3.up * height / 2.0f);
    }
    
    private void HideMeshComponents()
    {
        _meshRenderer.hideFlags = HideFlags.HideInInspector;
        _meshFilter.hideFlags = HideFlags.HideInInspector;
    }

    private void OnDrawGizmos()
    {
        // Todo: Move to GizmosDrawer
        
        /*Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(startPosition, 0.3f);
        
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(endPosition, 0.3f);*/
    }
}
