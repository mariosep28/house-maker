using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter), typeof(MeshCollider))]
public class Cube : MonoBehaviour
{
    [Range(1, 5)] public float width = 1;
    [Range(1, 5)] public float length = 1;
    [Range(1, 5)] public float height = 1;
    
    public Vector3 offset = new Vector3(0, 0, 0);

    public Color color;
    
    private Shape _shape;
    
    private MeshRenderer _meshRenderer;
    private MeshFilter _meshFilter;
    private MeshCollider _meshCollider;
    
    public Vector3 StartPosition => transform.position;
    
    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _meshFilter = GetComponent<MeshFilter>();
        _meshCollider = GetComponent<MeshCollider>();
        
        HideMeshComponents();
        
        UpdateMesh();
    }
    
    private void OnValidate()
    {
        if (_meshRenderer == null || _meshFilter == null)
        {
            _meshRenderer = GetComponent<MeshRenderer>();
            _meshFilter = GetComponent<MeshFilter>();
            _meshCollider = GetComponent<MeshCollider>();
        
            HideMeshComponents();    
        }
        
        UpdateMesh();
    }

    public void Init(float width, float height, float length)
    {
        this.width = width;
        this.height = height;
        this.length = length;
        
        UpdateMesh();
    }
    

    public void SetSize(float width, float height, float length)
    {
        this.width = width;
        this.height = height;
        this.length = length;
        
        UpdateMesh();
    }

    public void SetOffset(Vector3 offset)
    {
        this.offset = offset;
    }
    
    public void GenerateMesh()
    {
        float halfHeight = height / 2;
        float halfWidth = width / 2;
        float halfLength = length / 2;
        
        _shape = new Shape();
        
        // Create quads
        // top
        _shape.CreateQuad(new Vector3(0, halfHeight, 0), new Vector3(halfWidth, 0, 0), new Vector3(0, 0, halfLength), offset);
        // bottom
        _shape.CreateQuad(new Vector3(0, -halfHeight, 0), new Vector3(-halfWidth, 0, 0), new Vector3(0, 0, halfLength), offset);
        // sides
        _shape.CreateQuad(new Vector3(-halfWidth, 0, 0), new Vector3(0, 0, -halfLength), new Vector3(0, halfHeight, 0), offset); // left
        _shape.CreateQuad(new Vector3(halfWidth, 0, 0), new Vector3(0, 0, halfLength), new Vector3(0, halfHeight, 0), offset); // right
        _shape.CreateQuad(new Vector3(0, 0, -halfLength), new Vector3(halfWidth, 0, 0), new Vector3(0, halfHeight, 0), offset); // front
        _shape.CreateQuad(new Vector3(0, 0, halfLength), new Vector3(-halfWidth, 0, 0), new Vector3(0, halfHeight, 0), offset); // back
        
        Mesh mesh = new Mesh();

        mesh.vertices = _shape.vertices.ToArray();
        mesh.triangles = _shape.triangles.ToArray();
        mesh.normals = _shape.normals.ToArray();

        /*var colors = new Color[mesh.vertices.Length];

        for (int i = 0; i < 4; i++)
        {
            colors[i] = color;
        }
        
        for (int i = 4; i < colors.Length; i++)
        {
            colors[i] = Color.white;
        }

        mesh.colors = colors;*/
        
        _meshFilter.mesh = mesh;
        _meshRenderer.material = DefaultDataManager.Instance.DefaultMaterial;
        _meshCollider.sharedMesh = mesh;
        _meshCollider.convex = true;
    }
    
    public void UpdateMesh()
    {
        GenerateMesh();
    }
    
    private void HideMeshComponents()
    {
        _meshRenderer.hideFlags = HideFlags.HideInInspector;
        _meshFilter.hideFlags = HideFlags.HideInInspector;
    }
}
