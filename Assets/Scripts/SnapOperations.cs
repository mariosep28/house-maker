using UnityEngine;

public static class SnapOperations
{
    public static Vector3 LimitToStraightAnglePosition(Vector3 originPosition, Vector3 currentPosition)
    {
        Vector3 pointerDirection = (currentPosition - originPosition).normalized;

        Vector3 limitedToStraightAnglePosition = new Vector3();

        float dotProduct = Vector3.Dot(Vector3.right, pointerDirection);
        
        Debug.Log(dotProduct);
        
         if (dotProduct >= 0.5f || dotProduct <= -0.5f)
         {
             float xDiff = currentPosition.x - originPosition.x;

             limitedToStraightAnglePosition = originPosition + xDiff * Vector3.right;
         }
         else if (dotProduct < 0.5f && dotProduct >= 0f)
         {
             float zDiff = currentPosition.z - originPosition.z;

             limitedToStraightAnglePosition = originPosition + zDiff * Vector3.forward;
         }

        return limitedToStraightAnglePosition;
    }

    public static Vector3 SnapWhenCloseToStraightAngles(Vector3 originPosition, Vector3 currentPosition)
    {
        Vector3 pointerDirection = (currentPosition - originPosition).normalized;

        Vector3 limitedToStraightAnglePosition = new Vector3();

        float dotProduct = Vector3.Dot(Vector3.right, pointerDirection);
        
        //Debug.Log(dotProduct);
        
        if (dotProduct >= 0.998f || dotProduct <= -0.998f)
        {
            float xDiff = currentPosition.x - originPosition.x;

            limitedToStraightAnglePosition = originPosition + xDiff * Vector3.right;
            
            return limitedToStraightAnglePosition;
        }
        else if (dotProduct < 0.1f && dotProduct >= -0.1f)
        {
            float zDiff = currentPosition.z - originPosition.z;

            limitedToStraightAnglePosition = originPosition + zDiff * Vector3.forward;

            return limitedToStraightAnglePosition;
        }
        else
        {
            return currentPosition;
        }
    }
    
    public static Vector3 SnapPosition(Vector3 position)
    {
        Vector3 snapPosition = new Vector3();
        
        snapPosition.x = Mathf.Round(position.x);
        snapPosition.y = Mathf.Round(position.y);
        snapPosition.z = Mathf.Round(position.z);

        return snapPosition;
    }

    public static Vector3 SnapToMiddleObjectAxis(Vector3 position, GameObject gameObject, SnapAxis axis)
    {
        Bounds bounds = gameObject.GetComponent<MeshFilter>().sharedMesh.bounds;

        SnapAxis aaxis = SnapAxis.X;
        
        float middle;
        
        switch (axis)
        {
            case SnapAxis.X:
                middle = bounds.size.x / 2;
                position.x = middle;
                break;
            
            case SnapAxis.Y:
                middle = bounds.size.y / 2;
                position.y = middle;
                break;
            
            case SnapAxis.Z:
                middle = bounds.size.z / 2;
                position.z = middle;
                break;
        }

        return position;
    }
}
