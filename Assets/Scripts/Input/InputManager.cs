using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    private static UserControls _userControls;

    public static InputAction PrincipalAction => _userControls.UserInput.PrincipalAction;
    public static InputAction Accept => _userControls.UserInput.Accept;
    public static InputAction Cancel => _userControls.UserInput.Cancel;
    public static InputAction Undo => _userControls.UserInput.Undo;
    public static InputAction Redo => _userControls.UserInput.Redo;
    public static Vector2 CameraMovement => _userControls.UserInput.CameraMovement.ReadValue<Vector2>();
    public static float CameraRise => _userControls.UserInput.CameraRise.ReadValue<float>();
    public static Vector2 CameraRotation => _userControls.UserInput.CameraRotation.ReadValue<Vector2>();
    public static Vector2 CameraPan => _userControls.UserInput.CameraPan.ReadValue<Vector2>();
    public static float CameraZoom => _userControls.UserInput.CameraZoom.ReadValue<Vector2>().y;
    public static bool CameraRotationActive => _userControls.UserInput.CameraRotationActive.IsPressed();
    public static bool CameraPanActive => _userControls.UserInput.CameraPanActive.IsPressed();
    
    private void Awake()
    {
        _userControls = new UserControls();
    }

    private void OnEnable()
    {
        _userControls.Enable();
    }

    private void OnDisable()
    {
        _userControls.Disable();
    }
}
